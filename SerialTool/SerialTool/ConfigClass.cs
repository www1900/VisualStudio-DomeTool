﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialTool
{
    /// <summary>
    /// 配置文件读取出来的信息
    /// </summary>
    public class ConfigClass
    {
        /// <summary>
        /// 系统信息配置保存路径
        /// </summary>
        private string SysInfoPath
        {
            get 
            {
                return this.SysInfoPath;
            }

            set
            {
                this.SysInfoPath = SysInfoPath;
            }
        }

        /// <summary>
        /// 数组存放文件名
        /// </summary>
        public Byte[] file_name;
        /// <summary>
        /// 文件名字的长度
        /// </summary>
        public int FILE_NAME_LENGTH;
        /// <summary>
        /// 文件传输协议
        /// </summary>
        public FileProtocal fileprotocal;

        /// <summary>
        /// 在线升级IAP下载文件保存路径
        /// </summary>
        public string DownloadFilePath;

        /// <summary>
        /// 将系统需要保存到下一次启动的信息保存到文件中
        /// </summary>
        public void SaveConfigSys()
        {   
               
        }

        /// <summary>
        /// 从系统配置文件中读取出来配置信息
        /// </summary>
        public void ReadConfigSys()
        { 
        
        }

        public ConfigClass()
        {
            this.fileprotocal = FileProtocal.Ymodem;
        }


    }
}
