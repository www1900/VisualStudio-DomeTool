﻿namespace SerialTool
{
    partial class MainDialog
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDialog));
            this.Labe1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.SerialParityBit = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SerialStopBit = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SerialDataBits = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SerialButton = new System.Windows.Forms.Button();
            this.SerialBaudrate = new System.Windows.Forms.ComboBox();
            this.Baudrate = new System.Windows.Forms.Label();
            this.SerialPortNum = new System.Windows.Forms.ComboBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新搜索串口ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MainTabPage = new System.Windows.Forms.TabControl();
            this.串口设置 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.SerialSendButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.SendTimerPeriod = new System.Windows.Forms.TextBox();
            this.TimeSendCheck = new System.Windows.Forms.CheckBox();
            this.SendCheckASCII = new System.Windows.Forms.RadioButton();
            this.SendCheckHex = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.DisSendCheck = new System.Windows.Forms.CheckBox();
            this.DisTimeCheck = new System.Windows.Forms.CheckBox();
            this.SaveBuffButton = new System.Windows.Forms.Button();
            this.CleanBufferButton = new System.Windows.Forms.Button();
            this.ReceiveCheckHex = new System.Windows.Forms.RadioButton();
            this.ReceiveCheckASCII = new System.Windows.Forms.RadioButton();
            this.AutoLineBackFlag = new System.Windows.Forms.CheckBox();
            this.DiolagTopFlag = new System.Windows.Forms.CheckBox();
            this.基本控制 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button16 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.protocol_label = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.PresentID = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.PresentCall = new System.Windows.Forms.Button();
            this.preset_label = new System.Windows.Forms.Label();
            this.在线升级 = new System.Windows.Forms.TabPage();
            this.UpdateApp = new System.Windows.Forms.GroupBox();
            this.CancelDownFileButton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DownloadFileCount = new System.Windows.Forms.RichTextBox();
            this.DownloadFileButton = new System.Windows.Forms.Button();
            this.DownloadFileProgressBar = new System.Windows.Forms.ProgressBar();
            this.DownloadFileStatus = new System.Windows.Forms.TextBox();
            this.DownloadFileVersion = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DownloadFileProtocal = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DownloadFileOpenButton = new System.Windows.Forms.Button();
            this.DownloadFilePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.摄像机设置 = new System.Windows.Forms.TabPage();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.用户列表 = new System.Windows.Forms.TabPage();
            this.user_name_textBox = new System.Windows.Forms.TextBox();
            this.user_name_label = new System.Windows.Forms.Label();
            this.user_id_label = new System.Windows.Forms.Label();
            this.user_id_comboBox = new System.Windows.Forms.ComboBox();
            this.联系人BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.userDataSet = new SerialTool.userDataSet();
            this.user_job_textBox = new System.Windows.Forms.TextBox();
            this.user_company_textBox = new System.Windows.Forms.TextBox();
            this.user_phone_textBox = new System.Windows.Forms.TextBox();
            this.user_email_textBox = new System.Windows.Forms.TextBox();
            this.user_surname_textBox = new System.Windows.Forms.TextBox();
            this.user_job_label = new System.Windows.Forms.Label();
            this.user_company_label = new System.Windows.Forms.Label();
            this.user_phone_label = new System.Windows.Forms.Label();
            this.user_emal_label = new System.Windows.Forms.Label();
            this.user_surname_label = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SerialDataSend = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SerialDataReceived = new System.Windows.Forms.RichTextBox();
            this.SerialPort = new System.IO.Ports.SerialPort(this.components);
            this.SystemTimer = new System.Windows.Forms.Timer(this.components);
            this.DownloadFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SerialDataFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SendTimer = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.TimeStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.联系人TableAdapter = new SerialTool.userDataSetTableAdapters.联系人TableAdapter();
            this.groupBox1.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.MainTabPage.SuspendLayout();
            this.串口设置.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.基本控制.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.在线升级.SuspendLayout();
            this.UpdateApp.SuspendLayout();
            this.摄像机设置.SuspendLayout();
            this.用户列表.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.联系人BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userDataSet)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Labe1
            // 
            this.Labe1.AutoSize = true;
            this.Labe1.Location = new System.Drawing.Point(88, 17);
            this.Labe1.Name = "Labe1";
            this.Labe1.Size = new System.Drawing.Size(56, 14);
            this.Labe1.TabIndex = 0;
            this.Labe1.Text = "串口号:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.SerialParityBit);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.SerialStopBit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.SerialDataBits);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.SerialButton);
            this.groupBox1.Controls.Add(this.SerialBaudrate);
            this.groupBox1.Controls.Add(this.Baudrate);
            this.groupBox1.Controls.Add(this.SerialPortNum);
            this.groupBox1.Controls.Add(this.Labe1);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(3, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(820, 40);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 14);
            this.label12.TabIndex = 11;
            this.label12.Text = "串口设置：";
            // 
            // SerialParityBit
            // 
            this.SerialParityBit.FormattingEnabled = true;
            this.SerialParityBit.Items.AddRange(new object[] {
            "奇",
            "偶",
            "无"});
            this.SerialParityBit.Location = new System.Drawing.Point(618, 14);
            this.SerialParityBit.Name = "SerialParityBit";
            this.SerialParityBit.Size = new System.Drawing.Size(47, 22);
            this.SerialParityBit.TabIndex = 10;
            this.SerialParityBit.Text = "无";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(562, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "校验位:";
            // 
            // SerialStopBit
            // 
            this.SerialStopBit.FormattingEnabled = true;
            this.SerialStopBit.Items.AddRange(new object[] {
            "1",
            "1.5",
            "2"});
            this.SerialStopBit.Location = new System.Drawing.Point(502, 15);
            this.SerialStopBit.Name = "SerialStopBit";
            this.SerialStopBit.Size = new System.Drawing.Size(48, 22);
            this.SerialStopBit.TabIndex = 8;
            this.SerialStopBit.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(450, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 7;
            this.label1.Text = "停止位:";
            // 
            // SerialDataBits
            // 
            this.SerialDataBits.FormattingEnabled = true;
            this.SerialDataBits.Items.AddRange(new object[] {
            "8",
            "9",
            "10"});
            this.SerialDataBits.Location = new System.Drawing.Point(408, 15);
            this.SerialDataBits.Name = "SerialDataBits";
            this.SerialDataBits.Size = new System.Drawing.Size(34, 22);
            this.SerialDataBits.TabIndex = 6;
            this.SerialDataBits.Text = "8";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(352, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 5;
            this.label3.Text = "数据位:";
            // 
            // SerialButton
            // 
            this.SerialButton.Location = new System.Drawing.Point(727, 13);
            this.SerialButton.Name = "SerialButton";
            this.SerialButton.Size = new System.Drawing.Size(75, 23);
            this.SerialButton.TabIndex = 4;
            this.SerialButton.Text = "打开";
            this.SerialButton.UseVisualStyleBackColor = true;
            this.SerialButton.Click += new System.EventHandler(this.SerialButton_Click);
            // 
            // SerialBaudrate
            // 
            this.SerialBaudrate.FormattingEnabled = true;
            this.SerialBaudrate.Items.AddRange(new object[] {
            "1200",
            "1800",
            "2400",
            "3600",
            "4800",
            "7200",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "57600",
            "115200",
            "230400",
            "460800",
            "614400",
            "921600",
            "1228800"});
            this.SerialBaudrate.Location = new System.Drawing.Point(264, 14);
            this.SerialBaudrate.Name = "SerialBaudrate";
            this.SerialBaudrate.Size = new System.Drawing.Size(77, 22);
            this.SerialBaudrate.TabIndex = 3;
            this.SerialBaudrate.Text = "115200";
            // 
            // Baudrate
            // 
            this.Baudrate.AutoSize = true;
            this.Baudrate.Location = new System.Drawing.Point(211, 18);
            this.Baudrate.Name = "Baudrate";
            this.Baudrate.Size = new System.Drawing.Size(56, 14);
            this.Baudrate.TabIndex = 2;
            this.Baudrate.Text = "波特率:";
            // 
            // SerialPortNum
            // 
            this.SerialPortNum.FormattingEnabled = true;
            this.SerialPortNum.Location = new System.Drawing.Point(147, 14);
            this.SerialPortNum.Name = "SerialPortNum";
            this.SerialPortNum.Size = new System.Drawing.Size(54, 22);
            this.SerialPortNum.TabIndex = 1;
            // 
            // menuStrip
            // 
            this.menuStrip.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Underline);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.编辑ToolStripMenuItem,
            this.关于ToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(832, 25);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "MainMenu";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.退出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 编辑ToolStripMenuItem
            // 
            this.编辑ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.重新搜索串口ToolStripMenuItem});
            this.编辑ToolStripMenuItem.Name = "编辑ToolStripMenuItem";
            this.编辑ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.编辑ToolStripMenuItem.Text = "编辑";
            // 
            // 重新搜索串口ToolStripMenuItem
            // 
            this.重新搜索串口ToolStripMenuItem.Name = "重新搜索串口ToolStripMenuItem";
            this.重新搜索串口ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.重新搜索串口ToolStripMenuItem.Text = "重新搜索串口";
            this.重新搜索串口ToolStripMenuItem.Click += new System.EventHandler(this.重新搜索串口ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于ToolStripMenuItem1});
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.关于ToolStripMenuItem.Text = "关于";
            // 
            // 关于ToolStripMenuItem1
            // 
            this.关于ToolStripMenuItem1.Name = "关于ToolStripMenuItem1";
            this.关于ToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.关于ToolStripMenuItem1.Text = "关于";
            this.关于ToolStripMenuItem1.Click += new System.EventHandler(this.关于ToolStripMenuItem1_Click);
            // 
            // MainTabPage
            // 
            this.MainTabPage.Controls.Add(this.串口设置);
            this.MainTabPage.Controls.Add(this.基本控制);
            this.MainTabPage.Controls.Add(this.在线升级);
            this.MainTabPage.Controls.Add(this.摄像机设置);
            this.MainTabPage.Controls.Add(this.用户列表);
            this.MainTabPage.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MainTabPage.Location = new System.Drawing.Point(372, 73);
            this.MainTabPage.Name = "MainTabPage";
            this.MainTabPage.SelectedIndex = 0;
            this.MainTabPage.Size = new System.Drawing.Size(451, 354);
            this.MainTabPage.TabIndex = 3;
            this.MainTabPage.Selected += new System.Windows.Forms.TabControlEventHandler(this.MainTabPage_Selected);
            // 
            // 串口设置
            // 
            this.串口设置.Controls.Add(this.groupBox5);
            this.串口设置.Controls.Add(this.groupBox4);
            this.串口设置.Location = new System.Drawing.Point(4, 24);
            this.串口设置.Name = "串口设置";
            this.串口设置.Padding = new System.Windows.Forms.Padding(3);
            this.串口设置.Size = new System.Drawing.Size(443, 326);
            this.串口设置.TabIndex = 0;
            this.串口设置.Text = "串口设置";
            this.串口设置.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.SerialSendButton);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.SendTimerPeriod);
            this.groupBox5.Controls.Add(this.TimeSendCheck);
            this.groupBox5.Controls.Add(this.SendCheckASCII);
            this.groupBox5.Controls.Add(this.SendCheckHex);
            this.groupBox5.Location = new System.Drawing.Point(213, 9);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(181, 302);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "发送设置";
            // 
            // SerialSendButton
            // 
            this.SerialSendButton.Location = new System.Drawing.Point(18, 141);
            this.SerialSendButton.Name = "SerialSendButton";
            this.SerialSendButton.Size = new System.Drawing.Size(75, 23);
            this.SerialSendButton.TabIndex = 5;
            this.SerialSendButton.Text = "发送";
            this.SerialSendButton.UseVisualStyleBackColor = true;
            this.SerialSendButton.Click += new System.EventHandler(this.SerialSendButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(73, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 14);
            this.label11.TabIndex = 4;
            this.label11.Text = "ms";
            // 
            // SendTimerPeriod
            // 
            this.SendTimerPeriod.Location = new System.Drawing.Point(19, 100);
            this.SendTimerPeriod.Name = "SendTimerPeriod";
            this.SendTimerPeriod.Size = new System.Drawing.Size(48, 23);
            this.SendTimerPeriod.TabIndex = 3;
            this.SendTimerPeriod.TextChanged += new System.EventHandler(this.SendTimerPeriod_TextChanged);
            // 
            // TimeSendCheck
            // 
            this.TimeSendCheck.AutoSize = true;
            this.TimeSendCheck.Location = new System.Drawing.Point(20, 65);
            this.TimeSendCheck.Name = "TimeSendCheck";
            this.TimeSendCheck.Size = new System.Drawing.Size(82, 18);
            this.TimeSendCheck.TabIndex = 2;
            this.TimeSendCheck.Text = "定时发送";
            this.TimeSendCheck.UseVisualStyleBackColor = true;
            this.TimeSendCheck.CheckedChanged += new System.EventHandler(this.TimeSendCheck_CheckedChanged);
            // 
            // SendCheckASCII
            // 
            this.SendCheckASCII.AutoSize = true;
            this.SendCheckASCII.Checked = true;
            this.SendCheckASCII.Location = new System.Drawing.Point(20, 29);
            this.SendCheckASCII.Name = "SendCheckASCII";
            this.SendCheckASCII.Size = new System.Drawing.Size(60, 18);
            this.SendCheckASCII.TabIndex = 1;
            this.SendCheckASCII.TabStop = true;
            this.SendCheckASCII.Text = "ASCII";
            this.SendCheckASCII.UseVisualStyleBackColor = true;
            this.SendCheckASCII.CheckedChanged += new System.EventHandler(this.SendCheckASCII_CheckedChanged);
            // 
            // SendCheckHex
            // 
            this.SendCheckHex.AutoSize = true;
            this.SendCheckHex.Location = new System.Drawing.Point(109, 29);
            this.SendCheckHex.Name = "SendCheckHex";
            this.SendCheckHex.Size = new System.Drawing.Size(46, 18);
            this.SendCheckHex.TabIndex = 0;
            this.SendCheckHex.Text = "Hex";
            this.SendCheckHex.UseVisualStyleBackColor = true;
            this.SendCheckHex.CheckedChanged += new System.EventHandler(this.SendCheckHex_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.DisSendCheck);
            this.groupBox4.Controls.Add(this.DisTimeCheck);
            this.groupBox4.Controls.Add(this.SaveBuffButton);
            this.groupBox4.Controls.Add(this.CleanBufferButton);
            this.groupBox4.Controls.Add(this.ReceiveCheckHex);
            this.groupBox4.Controls.Add(this.ReceiveCheckASCII);
            this.groupBox4.Controls.Add(this.AutoLineBackFlag);
            this.groupBox4.Controls.Add(this.DiolagTopFlag);
            this.groupBox4.Location = new System.Drawing.Point(10, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(188, 302);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "接收设置";
            // 
            // DisSendCheck
            // 
            this.DisSendCheck.AutoSize = true;
            this.DisSendCheck.Location = new System.Drawing.Point(21, 100);
            this.DisSendCheck.Name = "DisSendCheck";
            this.DisSendCheck.Size = new System.Drawing.Size(82, 18);
            this.DisSendCheck.TabIndex = 11;
            this.DisSendCheck.Text = "显示发送";
            this.DisSendCheck.UseVisualStyleBackColor = true;
            this.DisSendCheck.CheckedChanged += new System.EventHandler(this.DisSendCheck_CheckedChanged);
            // 
            // DisTimeCheck
            // 
            this.DisTimeCheck.AutoSize = true;
            this.DisTimeCheck.Location = new System.Drawing.Point(21, 63);
            this.DisTimeCheck.Name = "DisTimeCheck";
            this.DisTimeCheck.Size = new System.Drawing.Size(82, 18);
            this.DisTimeCheck.TabIndex = 10;
            this.DisTimeCheck.Text = "显示时间";
            this.DisTimeCheck.UseVisualStyleBackColor = true;
            this.DisTimeCheck.CheckedChanged += new System.EventHandler(this.DisTimeCheck_CheckedChanged);
            // 
            // SaveBuffButton
            // 
            this.SaveBuffButton.Location = new System.Drawing.Point(20, 258);
            this.SaveBuffButton.Name = "SaveBuffButton";
            this.SaveBuffButton.Size = new System.Drawing.Size(75, 23);
            this.SaveBuffButton.TabIndex = 9;
            this.SaveBuffButton.Text = "保存数据";
            this.SaveBuffButton.UseVisualStyleBackColor = true;
            this.SaveBuffButton.Click += new System.EventHandler(this.SaveBuffButton_Click);
            // 
            // CleanBufferButton
            // 
            this.CleanBufferButton.Location = new System.Drawing.Point(19, 214);
            this.CleanBufferButton.Name = "CleanBufferButton";
            this.CleanBufferButton.Size = new System.Drawing.Size(75, 23);
            this.CleanBufferButton.TabIndex = 8;
            this.CleanBufferButton.Text = "清空";
            this.CleanBufferButton.UseVisualStyleBackColor = true;
            this.CleanBufferButton.Click += new System.EventHandler(this.CleanBufferButton_Click);
            // 
            // ReceiveCheckHex
            // 
            this.ReceiveCheckHex.AutoSize = true;
            this.ReceiveCheckHex.Location = new System.Drawing.Point(122, 29);
            this.ReceiveCheckHex.Name = "ReceiveCheckHex";
            this.ReceiveCheckHex.Size = new System.Drawing.Size(46, 18);
            this.ReceiveCheckHex.TabIndex = 7;
            this.ReceiveCheckHex.Text = "Hex";
            this.ReceiveCheckHex.UseVisualStyleBackColor = true;
            this.ReceiveCheckHex.CheckedChanged += new System.EventHandler(this.ReceiveCheckHex_CheckedChanged);
            // 
            // ReceiveCheckASCII
            // 
            this.ReceiveCheckASCII.AutoSize = true;
            this.ReceiveCheckASCII.Checked = true;
            this.ReceiveCheckASCII.Location = new System.Drawing.Point(20, 29);
            this.ReceiveCheckASCII.Name = "ReceiveCheckASCII";
            this.ReceiveCheckASCII.Size = new System.Drawing.Size(60, 18);
            this.ReceiveCheckASCII.TabIndex = 6;
            this.ReceiveCheckASCII.TabStop = true;
            this.ReceiveCheckASCII.Text = "ASCII";
            this.ReceiveCheckASCII.UseVisualStyleBackColor = true;
            this.ReceiveCheckASCII.CheckedChanged += new System.EventHandler(this.ReceiveCheckASCII_CheckedChanged);
            // 
            // AutoLineBackFlag
            // 
            this.AutoLineBackFlag.AutoSize = true;
            this.AutoLineBackFlag.Location = new System.Drawing.Point(22, 176);
            this.AutoLineBackFlag.Name = "AutoLineBackFlag";
            this.AutoLineBackFlag.Size = new System.Drawing.Size(82, 18);
            this.AutoLineBackFlag.TabIndex = 5;
            this.AutoLineBackFlag.Text = "自动换行";
            this.AutoLineBackFlag.UseVisualStyleBackColor = true;
            this.AutoLineBackFlag.CheckedChanged += new System.EventHandler(this.AutoLineBackFlag_CheckedChanged);
            // 
            // DiolagTopFlag
            // 
            this.DiolagTopFlag.AutoSize = true;
            this.DiolagTopFlag.Location = new System.Drawing.Point(21, 142);
            this.DiolagTopFlag.Name = "DiolagTopFlag";
            this.DiolagTopFlag.Size = new System.Drawing.Size(82, 18);
            this.DiolagTopFlag.TabIndex = 4;
            this.DiolagTopFlag.Text = "窗口置顶";
            this.DiolagTopFlag.UseVisualStyleBackColor = true;
            this.DiolagTopFlag.CheckedChanged += new System.EventHandler(this.DiolagTopFlag_CheckedChanged);
            // 
            // 基本控制
            // 
            this.基本控制.Controls.Add(this.groupBox7);
            this.基本控制.Controls.Add(this.groupBox6);
            this.基本控制.Location = new System.Drawing.Point(4, 24);
            this.基本控制.Name = "基本控制";
            this.基本控制.Padding = new System.Windows.Forms.Padding(3);
            this.基本控制.Size = new System.Drawing.Size(443, 326);
            this.基本控制.TabIndex = 1;
            this.基本控制.Text = "基本控制";
            this.基本控制.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button16);
            this.groupBox7.Controls.Add(this.button13);
            this.groupBox7.Location = new System.Drawing.Point(234, 5);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(203, 312);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "特殊设置";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(13, 55);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(113, 23);
            this.button16.TabIndex = 1;
            this.button16.Text = "恢复出厂设置";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(10, 22);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(116, 23);
            this.button13.TabIndex = 0;
            this.button13.Text = "系统重启";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button15);
            this.groupBox6.Controls.Add(this.button14);
            this.groupBox6.Controls.Add(this.comboBox9);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.richTextBox1);
            this.groupBox6.Controls.Add(this.comboBox1);
            this.groupBox6.Controls.Add(this.protocol_label);
            this.groupBox6.Controls.Add(this.button10);
            this.groupBox6.Controls.Add(this.button9);
            this.groupBox6.Controls.Add(this.button8);
            this.groupBox6.Controls.Add(this.button7);
            this.groupBox6.Controls.Add(this.button6);
            this.groupBox6.Controls.Add(this.button5);
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.button4);
            this.groupBox6.Controls.Add(this.PresentID);
            this.groupBox6.Controls.Add(this.button3);
            this.groupBox6.Controls.Add(this.button2);
            this.groupBox6.Controls.Add(this.PresentCall);
            this.groupBox6.Controls.Add(this.preset_label);
            this.groupBox6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(220, 311);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "预置点";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(123, 275);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 19;
            this.button15.Text = "关闭";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(20, 275);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 18;
            this.button14.Text = "打开";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // comboBox9
            // 
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(80, 247);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBox9.Size = new System.Drawing.Size(112, 22);
            this.comboBox9.TabIndex = 17;
            this.comboBox9.Text = "1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.Location = new System.Drawing.Point(18, 250);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 14);
            this.label20.TabIndex = 16;
            this.label20.Text = "辅助点:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.richTextBox1.Location = new System.Drawing.Point(86, 165);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(48, 33);
            this.richTextBox1.TabIndex = 15;
            this.richTextBox1.Text = "54";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "PelcoD",
            "Pelcop"});
            this.comboBox1.Location = new System.Drawing.Point(87, 56);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBox1.Size = new System.Drawing.Size(113, 22);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.Text = "PelcoD";
            // 
            // protocol_label
            // 
            this.protocol_label.AutoSize = true;
            this.protocol_label.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.protocol_label.Location = new System.Drawing.Point(15, 60);
            this.protocol_label.Name = "protocol_label";
            this.protocol_label.Size = new System.Drawing.Size(56, 14);
            this.protocol_label.TabIndex = 13;
            this.protocol_label.Text = "协  议:";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(153, 209);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(48, 27);
            this.button10.TabIndex = 12;
            this.button10.Text = "右下";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(87, 209);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(48, 27);
            this.button9.TabIndex = 11;
            this.button9.Text = "下";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(21, 209);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 27);
            this.button8.TabIndex = 10;
            this.button8.Text = "左下";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(151, 170);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(48, 22);
            this.button7.TabIndex = 9;
            this.button7.Text = "右";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(20, 170);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 22);
            this.button6.TabIndex = 8;
            this.button6.Text = "左";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(152, 126);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(48, 26);
            this.button5.TabIndex = 7;
            this.button5.Text = "右上";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(84, 126);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 26);
            this.button1.TabIndex = 6;
            this.button1.Text = "上";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(20, 126);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 26);
            this.button4.TabIndex = 5;
            this.button4.Text = "左上";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // PresentID
            // 
            this.PresentID.FormattingEnabled = true;
            this.PresentID.Location = new System.Drawing.Point(87, 22);
            this.PresentID.Name = "PresentID";
            this.PresentID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PresentID.Size = new System.Drawing.Size(112, 22);
            this.PresentID.TabIndex = 4;
            this.PresentID.Text = "1";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(152, 87);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(48, 24);
            this.button3.TabIndex = 3;
            this.button3.Text = "删除";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(86, 87);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(48, 24);
            this.button2.TabIndex = 2;
            this.button2.Text = "设置";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PresentCall
            // 
            this.PresentCall.Location = new System.Drawing.Point(19, 92);
            this.PresentCall.Name = "PresentCall";
            this.PresentCall.Size = new System.Drawing.Size(50, 24);
            this.PresentCall.TabIndex = 1;
            this.PresentCall.Text = "调用";
            this.PresentCall.UseVisualStyleBackColor = true;
            this.PresentCall.Click += new System.EventHandler(this.PresentCall_Click);
            // 
            // preset_label
            // 
            this.preset_label.AutoSize = true;
            this.preset_label.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.preset_label.Location = new System.Drawing.Point(16, 26);
            this.preset_label.Name = "preset_label";
            this.preset_label.Size = new System.Drawing.Size(56, 14);
            this.preset_label.TabIndex = 0;
            this.preset_label.Text = "预置点:";
            // 
            // 在线升级
            // 
            this.在线升级.Controls.Add(this.UpdateApp);
            this.在线升级.Location = new System.Drawing.Point(4, 24);
            this.在线升级.Name = "在线升级";
            this.在线升级.Padding = new System.Windows.Forms.Padding(3);
            this.在线升级.Size = new System.Drawing.Size(443, 326);
            this.在线升级.TabIndex = 2;
            this.在线升级.Text = "在线升级";
            this.在线升级.UseVisualStyleBackColor = true;
            // 
            // UpdateApp
            // 
            this.UpdateApp.Controls.Add(this.CancelDownFileButton);
            this.UpdateApp.Controls.Add(this.label10);
            this.UpdateApp.Controls.Add(this.label9);
            this.UpdateApp.Controls.Add(this.DownloadFileCount);
            this.UpdateApp.Controls.Add(this.DownloadFileButton);
            this.UpdateApp.Controls.Add(this.DownloadFileProgressBar);
            this.UpdateApp.Controls.Add(this.DownloadFileStatus);
            this.UpdateApp.Controls.Add(this.DownloadFileVersion);
            this.UpdateApp.Controls.Add(this.label8);
            this.UpdateApp.Controls.Add(this.label7);
            this.UpdateApp.Controls.Add(this.label6);
            this.UpdateApp.Controls.Add(this.DownloadFileProtocal);
            this.UpdateApp.Controls.Add(this.label5);
            this.UpdateApp.Controls.Add(this.DownloadFileOpenButton);
            this.UpdateApp.Controls.Add(this.DownloadFilePath);
            this.UpdateApp.Controls.Add(this.label4);
            this.UpdateApp.Location = new System.Drawing.Point(6, 6);
            this.UpdateApp.Name = "UpdateApp";
            this.UpdateApp.Size = new System.Drawing.Size(431, 188);
            this.UpdateApp.TabIndex = 1;
            this.UpdateApp.TabStop = false;
            this.UpdateApp.Text = "IAP";
            // 
            // CancelDownFileButton
            // 
            this.CancelDownFileButton.Enabled = false;
            this.CancelDownFileButton.Location = new System.Drawing.Point(335, 79);
            this.CancelDownFileButton.Name = "CancelDownFileButton";
            this.CancelDownFileButton.Size = new System.Drawing.Size(75, 23);
            this.CancelDownFileButton.TabIndex = 15;
            this.CancelDownFileButton.Text = "取消烧写";
            this.CancelDownFileButton.UseVisualStyleBackColor = true;
            this.CancelDownFileButton.Click += new System.EventHandler(this.CancelDownFileButton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(333, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 14);
            this.label10.TabIndex = 14;
            this.label10.Text = "烧写次数：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(434, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 14);
            this.label9.TabIndex = 13;
            this.label9.Text = "  ";
            // 
            // DownloadFileCount
            // 
            this.DownloadFileCount.Enabled = false;
            this.DownloadFileCount.Font = new System.Drawing.Font("宋体", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DownloadFileCount.Location = new System.Drawing.Point(335, 128);
            this.DownloadFileCount.Name = "DownloadFileCount";
            this.DownloadFileCount.Size = new System.Drawing.Size(74, 51);
            this.DownloadFileCount.TabIndex = 12;
            this.DownloadFileCount.Text = "0";
            // 
            // DownloadFileButton
            // 
            this.DownloadFileButton.Location = new System.Drawing.Point(334, 50);
            this.DownloadFileButton.Name = "DownloadFileButton";
            this.DownloadFileButton.Size = new System.Drawing.Size(75, 23);
            this.DownloadFileButton.TabIndex = 11;
            this.DownloadFileButton.Text = "烧写程序";
            this.DownloadFileButton.UseVisualStyleBackColor = true;
            this.DownloadFileButton.Click += new System.EventHandler(this.DownloadFileButton_Click);
            // 
            // DownloadFileProgressBar
            // 
            this.DownloadFileProgressBar.Location = new System.Drawing.Point(82, 146);
            this.DownloadFileProgressBar.Name = "DownloadFileProgressBar";
            this.DownloadFileProgressBar.Size = new System.Drawing.Size(235, 23);
            this.DownloadFileProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.DownloadFileProgressBar.TabIndex = 10;
            // 
            // DownloadFileStatus
            // 
            this.DownloadFileStatus.Location = new System.Drawing.Point(82, 114);
            this.DownloadFileStatus.Name = "DownloadFileStatus";
            this.DownloadFileStatus.Size = new System.Drawing.Size(235, 23);
            this.DownloadFileStatus.TabIndex = 9;
            this.DownloadFileStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DownloadFileVersion
            // 
            this.DownloadFileVersion.Location = new System.Drawing.Point(82, 81);
            this.DownloadFileVersion.Name = "DownloadFileVersion";
            this.DownloadFileVersion.Size = new System.Drawing.Size(235, 23);
            this.DownloadFileVersion.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 119);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 14);
            this.label8.TabIndex = 7;
            this.label8.Text = "烧写状态：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 6;
            this.label7.Text = "下载进度：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 5;
            this.label6.Text = "版本号：";
            // 
            // DownloadFileProtocal
            // 
            this.DownloadFileProtocal.FormattingEnabled = true;
            this.DownloadFileProtocal.Items.AddRange(new object[] {
            "Xmodem",
            "Ymodem",
            "Zmodem",
            "Kermit"});
            this.DownloadFileProtocal.Location = new System.Drawing.Point(82, 51);
            this.DownloadFileProtocal.Name = "DownloadFileProtocal";
            this.DownloadFileProtocal.Size = new System.Drawing.Size(80, 22);
            this.DownloadFileProtocal.TabIndex = 4;
            this.DownloadFileProtocal.Text = "Ymodem";
            this.DownloadFileProtocal.SelectedIndexChanged += new System.EventHandler(this.DownloadFileProtocal_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 3;
            this.label5.Text = "烧写协议：";
            // 
            // DownloadFileOpenButton
            // 
            this.DownloadFileOpenButton.Location = new System.Drawing.Point(334, 18);
            this.DownloadFileOpenButton.Name = "DownloadFileOpenButton";
            this.DownloadFileOpenButton.Size = new System.Drawing.Size(75, 23);
            this.DownloadFileOpenButton.TabIndex = 2;
            this.DownloadFileOpenButton.Text = "打开文件";
            this.DownloadFileOpenButton.UseVisualStyleBackColor = true;
            this.DownloadFileOpenButton.Click += new System.EventHandler(this.DownloadFileOpenButton_Click);
            // 
            // DownloadFilePath
            // 
            this.DownloadFilePath.Location = new System.Drawing.Point(82, 23);
            this.DownloadFilePath.Name = "DownloadFilePath";
            this.DownloadFilePath.Size = new System.Drawing.Size(235, 23);
            this.DownloadFilePath.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "烧写文件：";
            // 
            // 摄像机设置
            // 
            this.摄像机设置.Controls.Add(this.button23);
            this.摄像机设置.Controls.Add(this.button22);
            this.摄像机设置.Controls.Add(this.button21);
            this.摄像机设置.Controls.Add(this.button20);
            this.摄像机设置.Controls.Add(this.button19);
            this.摄像机设置.Controls.Add(this.button18);
            this.摄像机设置.Controls.Add(this.button17);
            this.摄像机设置.Controls.Add(this.comboBox10);
            this.摄像机设置.Controls.Add(this.button12);
            this.摄像机设置.Controls.Add(this.button11);
            this.摄像机设置.Controls.Add(this.comboBox8);
            this.摄像机设置.Controls.Add(this.comboBox7);
            this.摄像机设置.Controls.Add(this.comboBox6);
            this.摄像机设置.Controls.Add(this.comboBox5);
            this.摄像机设置.Controls.Add(this.comboBox4);
            this.摄像机设置.Controls.Add(this.comboBox3);
            this.摄像机设置.Controls.Add(this.comboBox2);
            this.摄像机设置.Controls.Add(this.label19);
            this.摄像机设置.Controls.Add(this.label18);
            this.摄像机设置.Controls.Add(this.label17);
            this.摄像机设置.Controls.Add(this.label16);
            this.摄像机设置.Controls.Add(this.label15);
            this.摄像机设置.Controls.Add(this.label14);
            this.摄像机设置.Controls.Add(this.label13);
            this.摄像机设置.Location = new System.Drawing.Point(4, 24);
            this.摄像机设置.Name = "摄像机设置";
            this.摄像机设置.Padding = new System.Windows.Forms.Padding(3);
            this.摄像机设置.Size = new System.Drawing.Size(443, 326);
            this.摄像机设置.TabIndex = 3;
            this.摄像机设置.Text = "摄像机设置";
            this.摄像机设置.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(222, 264);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(60, 23);
            this.button23.TabIndex = 23;
            this.button23.Text = "设置";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(222, 221);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(60, 23);
            this.button22.TabIndex = 22;
            this.button22.Text = "设置";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(222, 179);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(60, 23);
            this.button21.TabIndex = 21;
            this.button21.Text = "设置";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(222, 139);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(60, 23);
            this.button20.TabIndex = 20;
            this.button20.Text = "设置";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(222, 99);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(60, 23);
            this.button19.TabIndex = 19;
            this.button19.Text = "设置";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(222, 60);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(60, 23);
            this.button18.TabIndex = 18;
            this.button18.Text = "设置";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(222, 20);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(60, 23);
            this.button17.TabIndex = 17;
            this.button17.Text = "设置";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(302, 138);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(121, 22);
            this.comboBox10.TabIndex = 16;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(303, 61);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(120, 27);
            this.button12.TabIndex = 15;
            this.button12.Text = "设置配置信息";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(302, 20);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(121, 28);
            this.button11.TabIndex = 14;
            this.button11.Text = "读取配置信息";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(129, 264);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(60, 22);
            this.comboBox8.TabIndex = 13;
            this.comboBox8.Text = "0";
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(129, 222);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(60, 22);
            this.comboBox7.TabIndex = 12;
            this.comboBox7.Text = "0";
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(129, 179);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(60, 22);
            this.comboBox6.TabIndex = 11;
            this.comboBox6.Text = "0";
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(129, 138);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(60, 22);
            this.comboBox5.TabIndex = 10;
            this.comboBox5.Text = "0";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(129, 99);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(60, 22);
            this.comboBox4.TabIndex = 9;
            this.comboBox4.Text = "0";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(129, 61);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(60, 22);
            this.comboBox3.TabIndex = 8;
            this.comboBox3.Text = "0";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "-2",
            "-1",
            "0",
            "+1",
            "+2"});
            this.comboBox2.Location = new System.Drawing.Point(129, 22);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(60, 22);
            this.comboBox2.TabIndex = 7;
            this.comboBox2.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(24, 268);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 14);
            this.label19.TabIndex = 6;
            this.label19.Text = "彩条测试：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(24, 225);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 14);
            this.label18.TabIndex = 5;
            this.label18.Text = "增益：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(23, 181);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 14);
            this.label17.TabIndex = 4;
            this.label17.Text = "特效：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 139);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 14);
            this.label16.TabIndex = 3;
            this.label16.Text = "亮度级别：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 103);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 14);
            this.label15.TabIndex = 2;
            this.label15.Text = "色彩对比度：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 14);
            this.label14.TabIndex = 1;
            this.label14.Text = "白平衡级别：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "曝光级别：";
            // 
            // 用户列表
            // 
            this.用户列表.Controls.Add(this.user_name_textBox);
            this.用户列表.Controls.Add(this.user_name_label);
            this.用户列表.Controls.Add(this.user_id_label);
            this.用户列表.Controls.Add(this.user_id_comboBox);
            this.用户列表.Controls.Add(this.user_job_textBox);
            this.用户列表.Controls.Add(this.user_company_textBox);
            this.用户列表.Controls.Add(this.user_phone_textBox);
            this.用户列表.Controls.Add(this.user_email_textBox);
            this.用户列表.Controls.Add(this.user_surname_textBox);
            this.用户列表.Controls.Add(this.user_job_label);
            this.用户列表.Controls.Add(this.user_company_label);
            this.用户列表.Controls.Add(this.user_phone_label);
            this.用户列表.Controls.Add(this.user_emal_label);
            this.用户列表.Controls.Add(this.user_surname_label);
            this.用户列表.Cursor = System.Windows.Forms.Cursors.Cross;
            this.用户列表.Location = new System.Drawing.Point(4, 24);
            this.用户列表.Name = "用户列表";
            this.用户列表.Size = new System.Drawing.Size(443, 326);
            this.用户列表.TabIndex = 4;
            this.用户列表.Text = "用户列表";
            this.用户列表.UseVisualStyleBackColor = true;
            // 
            // user_name_textBox
            // 
            this.user_name_textBox.Location = new System.Drawing.Point(103, 116);
            this.user_name_textBox.Name = "user_name_textBox";
            this.user_name_textBox.Size = new System.Drawing.Size(100, 23);
            this.user_name_textBox.TabIndex = 13;
            // 
            // user_name_label
            // 
            this.user_name_label.AutoSize = true;
            this.user_name_label.Location = new System.Drawing.Point(35, 118);
            this.user_name_label.Name = "user_name_label";
            this.user_name_label.Size = new System.Drawing.Size(28, 14);
            this.user_name_label.TabIndex = 12;
            this.user_name_label.Text = "名:";
            // 
            // user_id_label
            // 
            this.user_id_label.AutoSize = true;
            this.user_id_label.Location = new System.Drawing.Point(36, 37);
            this.user_id_label.Name = "user_id_label";
            this.user_id_label.Size = new System.Drawing.Size(35, 14);
            this.user_id_label.TabIndex = 11;
            this.user_id_label.Text = "ID：";
            // 
            // user_id_comboBox
            // 
            this.user_id_comboBox.DataSource = this.联系人BindingSource;
            this.user_id_comboBox.DisplayMember = "姓氏";
            this.user_id_comboBox.FormattingEnabled = true;
            this.user_id_comboBox.Location = new System.Drawing.Point(104, 31);
            this.user_id_comboBox.Name = "user_id_comboBox";
            this.user_id_comboBox.Size = new System.Drawing.Size(100, 22);
            this.user_id_comboBox.TabIndex = 10;
            this.user_id_comboBox.SelectedValueChanged += new System.EventHandler(this.user_id_comboBox_SelectedValueChanged);
            // 
            // 联系人BindingSource
            // 
            this.联系人BindingSource.DataMember = "联系人";
            this.联系人BindingSource.DataSource = this.userDataSet;
            // 
            // userDataSet
            // 
            this.userDataSet.DataSetName = "userDataSet";
            this.userDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            this.userDataSet.Initialized += new System.EventHandler(this.userDataSet_Initialized);
            // 
            // user_job_textBox
            // 
            this.user_job_textBox.Location = new System.Drawing.Point(101, 283);
            this.user_job_textBox.Name = "user_job_textBox";
            this.user_job_textBox.Size = new System.Drawing.Size(100, 23);
            this.user_job_textBox.TabIndex = 9;
            // 
            // user_company_textBox
            // 
            this.user_company_textBox.Location = new System.Drawing.Point(101, 240);
            this.user_company_textBox.Name = "user_company_textBox";
            this.user_company_textBox.Size = new System.Drawing.Size(100, 23);
            this.user_company_textBox.TabIndex = 8;
            // 
            // user_phone_textBox
            // 
            this.user_phone_textBox.Location = new System.Drawing.Point(102, 200);
            this.user_phone_textBox.Name = "user_phone_textBox";
            this.user_phone_textBox.Size = new System.Drawing.Size(100, 23);
            this.user_phone_textBox.TabIndex = 7;
            // 
            // user_email_textBox
            // 
            this.user_email_textBox.Location = new System.Drawing.Point(102, 156);
            this.user_email_textBox.Name = "user_email_textBox";
            this.user_email_textBox.Size = new System.Drawing.Size(100, 23);
            this.user_email_textBox.TabIndex = 6;
            // 
            // user_surname_textBox
            // 
            this.user_surname_textBox.Location = new System.Drawing.Point(104, 76);
            this.user_surname_textBox.Name = "user_surname_textBox";
            this.user_surname_textBox.Size = new System.Drawing.Size(100, 23);
            this.user_surname_textBox.TabIndex = 5;
            // 
            // user_job_label
            // 
            this.user_job_label.AutoSize = true;
            this.user_job_label.Location = new System.Drawing.Point(35, 286);
            this.user_job_label.Name = "user_job_label";
            this.user_job_label.Size = new System.Drawing.Size(49, 14);
            this.user_job_label.TabIndex = 4;
            this.user_job_label.Text = "职务：";
            // 
            // user_company_label
            // 
            this.user_company_label.AutoSize = true;
            this.user_company_label.Location = new System.Drawing.Point(33, 246);
            this.user_company_label.Name = "user_company_label";
            this.user_company_label.Size = new System.Drawing.Size(49, 14);
            this.user_company_label.TabIndex = 3;
            this.user_company_label.Text = "公司：";
            // 
            // user_phone_label
            // 
            this.user_phone_label.AutoSize = true;
            this.user_phone_label.Location = new System.Drawing.Point(32, 202);
            this.user_phone_label.Name = "user_phone_label";
            this.user_phone_label.Size = new System.Drawing.Size(49, 14);
            this.user_phone_label.TabIndex = 2;
            this.user_phone_label.Text = "电话：";
            // 
            // user_emal_label
            // 
            this.user_emal_label.AutoSize = true;
            this.user_emal_label.Location = new System.Drawing.Point(35, 159);
            this.user_emal_label.Name = "user_emal_label";
            this.user_emal_label.Size = new System.Drawing.Size(49, 14);
            this.user_emal_label.TabIndex = 1;
            this.user_emal_label.Text = "邮件：";
            // 
            // user_surname_label
            // 
            this.user_surname_label.AutoSize = true;
            this.user_surname_label.Location = new System.Drawing.Point(35, 79);
            this.user_surname_label.Name = "user_surname_label";
            this.user_surname_label.Size = new System.Drawing.Size(35, 14);
            this.user_surname_label.TabIndex = 0;
            this.user_surname_label.Text = "姓：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SerialDataSend);
            this.groupBox3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(6, 320);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(360, 107);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "发送数据(十六进制数00-FF)";
            // 
            // SerialDataSend
            // 
            this.SerialDataSend.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SerialDataSend.Location = new System.Drawing.Point(6, 20);
            this.SerialDataSend.Name = "SerialDataSend";
            this.SerialDataSend.Size = new System.Drawing.Size(348, 74);
            this.SerialDataSend.TabIndex = 0;
            this.SerialDataSend.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SerialDataReceived);
            this.groupBox2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(5, 73);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(361, 240);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "接收区";
            // 
            // SerialDataReceived
            // 
            this.SerialDataReceived.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SerialDataReceived.HideSelection = false;
            this.SerialDataReceived.Location = new System.Drawing.Point(6, 20);
            this.SerialDataReceived.Name = "SerialDataReceived";
            this.SerialDataReceived.Size = new System.Drawing.Size(349, 207);
            this.SerialDataReceived.TabIndex = 0;
            this.SerialDataReceived.Text = "";
            // 
            // SerialPort
            // 
            this.SerialPort.BaudRate = 115200;
            this.SerialPort.DiscardNull = true;
            this.SerialPort.ReadTimeout = 1;
            // 
            // SystemTimer
            // 
            this.SystemTimer.Enabled = true;
            this.SystemTimer.Interval = 1000;
            this.SystemTimer.Tick += new System.EventHandler(this.SystemTimer_Tick);
            // 
            // SendTimer
            // 
            this.SendTimer.Tick += new System.EventHandler(this.SendTimer_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TimeStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 430);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(832, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // TimeStatusLabel
            // 
            this.TimeStatusLabel.Name = "TimeStatusLabel";
            this.TimeStatusLabel.Size = new System.Drawing.Size(56, 17);
            this.TimeStatusLabel.Text = "当前时间";
            // 
            // 联系人TableAdapter
            // 
            this.联系人TableAdapter.ClearBeforeFill = true;
            // 
            // MainDialog
            // 
            this.AllowDrop = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(832, 452);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.MainTabPage);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainDialog";
            this.Text = "串口工具助手";
            this.Load += new System.EventHandler(this.MainDialog_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.MainTabPage.ResumeLayout(false);
            this.串口设置.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.基本控制.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.在线升级.ResumeLayout(false);
            this.UpdateApp.ResumeLayout(false);
            this.UpdateApp.PerformLayout();
            this.摄像机设置.ResumeLayout(false);
            this.摄像机设置.PerformLayout();
            this.用户列表.ResumeLayout(false);
            this.用户列表.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.联系人BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userDataSet)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Labe1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Baudrate;
        private System.Windows.Forms.ComboBox SerialPortNum;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.TabControl MainTabPage;
        private System.Windows.Forms.TabPage 串口设置;
        private System.Windows.Forms.TabPage 基本控制;
        private System.Windows.Forms.ComboBox SerialBaudrate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SerialButton;
        private System.Windows.Forms.ComboBox SerialDataBits;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.IO.Ports.SerialPort SerialPort;
        private System.Windows.Forms.ComboBox SerialParityBit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox SerialStopBit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage 在线升级;
        private System.Windows.Forms.Timer SystemTimer;
        private System.Windows.Forms.OpenFileDialog DownloadFileDialog;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox SerialDataSend;
        private System.Windows.Forms.RichTextBox SerialDataReceived;
        private System.Windows.Forms.GroupBox UpdateApp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button DownloadFileOpenButton;
        private System.Windows.Forms.TextBox DownloadFilePath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox DownloadFileStatus;
        private System.Windows.Forms.TextBox DownloadFileVersion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox DownloadFileProtocal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton SendCheckHex;
        private System.Windows.Forms.CheckBox TimeSendCheck;
        private System.Windows.Forms.RadioButton SendCheckASCII;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox SendTimerPeriod;
        private System.Windows.Forms.CheckBox AutoLineBackFlag;
        private System.Windows.Forms.CheckBox DiolagTopFlag;
        private System.Windows.Forms.RadioButton ReceiveCheckHex;
        private System.Windows.Forms.RadioButton ReceiveCheckASCII;
        private System.Windows.Forms.Button SaveBuffButton;
        private System.Windows.Forms.Button CleanBufferButton;
        private System.Windows.Forms.SaveFileDialog SerialDataFileDialog;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem1;
        public System.Windows.Forms.Button DownloadFileButton;
        public System.Windows.Forms.ProgressBar DownloadFileProgressBar;
        public System.Windows.Forms.RichTextBox DownloadFileCount;
        private System.Windows.Forms.Button SerialSendButton;
        private System.Windows.Forms.Timer SendTimer;
        private System.Windows.Forms.CheckBox DisSendCheck;
        private System.Windows.Forms.CheckBox DisTimeCheck;
        private System.Windows.Forms.ToolStripMenuItem 重新搜索串口ToolStripMenuItem;
        private System.Windows.Forms.Button CancelDownFileButton;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button PresentCall;
        private System.Windows.Forms.Label preset_label;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox PresentID;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label protocol_label;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel TimeStatusLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage 摄像机设置;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TabPage 用户列表;
        private System.Windows.Forms.Label user_id_label;
        private System.Windows.Forms.ComboBox user_id_comboBox;
        private System.Windows.Forms.TextBox user_job_textBox;
        private System.Windows.Forms.TextBox user_company_textBox;
        private System.Windows.Forms.TextBox user_phone_textBox;
        private System.Windows.Forms.TextBox user_email_textBox;
        private System.Windows.Forms.TextBox user_surname_textBox;
        private System.Windows.Forms.Label user_job_label;
        private System.Windows.Forms.Label user_company_label;
        private System.Windows.Forms.Label user_phone_label;
        private System.Windows.Forms.Label user_emal_label;
        private System.Windows.Forms.Label user_surname_label;
        private userDataSet userDataSet;
        private System.Windows.Forms.BindingSource 联系人BindingSource;
        private userDataSetTableAdapters.联系人TableAdapter 联系人TableAdapter;
        private System.Windows.Forms.TextBox user_name_textBox;
        private System.Windows.Forms.Label user_name_label;
    }
}

