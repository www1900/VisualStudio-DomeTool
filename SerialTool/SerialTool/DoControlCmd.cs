﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialTool
{
    public class ControlCmdClass
    {
        //private byte Header;
        public byte CmdCount
        {
            get 
            {
                return CmdCount;
            }
            set
            {
                this.CmdCount = value;
            }
        }
        public byte[,] CmdData;

        public void GetCmdData(byte[] data, int num, ControlCmd controlcmd)
        {
            int index = (int)controlcmd;
            if (index > this.CmdCount)
                return;
            for (int i = 0; i < 4; i++)
            {
                data[i + num] = CmdData[index, i];
            }
        }
    }

    public class PelcoD: ControlCmdClass
    {
        public PelcoD()
        {
            CmdCount = 20;
            CmdData = new byte[20, 4]
            {
                {0x00,0x08,0x00,0xff}, //    Up,         //上 　　
                {0x00,0x10,0x00,0xff}, //    Down,       //下 　　
                {0x00,0x04,0xff,0x00}, //    Left,       //左 　　
                {0x00,0x02,0xff,0x00}, //    Right,      //右 
                    
                {0x00,0x0C,0xff,0xff}, //   UpLeft,     //上左
                {0x00,0x0A,0xff,0xff}, //   UpRight,    //上右
                {0x00,0x14,0xff,0xff}, //   DownLeft,   //下左
                {0x00,0x12,0xff,0xff}, //   DownRight,  //下右
  
                {0x00,0x40,0x00,0x00}, //   ZoomAdd,        //变倍+ 
                {0x00,0x20,0x00,0x00}, //   ZoomSub,        //变倍- 　
            　
                {0x00,0x80,0x00,0x00}, //   FocusNear,      //聚焦近 　　
                {0x01,0x00,0x00,0x00}, //   FocusFar,       //聚焦远　
                {0x04,0x00,0x00,0x05}, //   ApertureBig,    //光圈+
                {0x02,0x00,0x00,0x00}, //   ApertureSmall,  //光圈- 　        
　           
                {0x00,0x0b,0x00,0x01}, //   AuxOff,     //辅助开关关 　
                {0x00,0x09,0x00,0x01}, //   AuxOn,      //辅助开关开
                                            
                {0x00,0x07,0x00,0x01}, //   CallPos,    //调用预置点
                {0x01,0x00,0x03,0x00}, //   SetPos,     //设置预置点
                {0x01,0x00,0x05,0x00}, //    DellPos,    //删除预置点
                {0x01,0x00,0x00,0x00}  //    CallStop,   //停止
            };
        }
    }

    public class PelcoP : ControlCmdClass
    {
        public PelcoP()
        {
            CmdCount = 20;
            CmdData = new byte[20, 4]
            {
                {0x00,0x08,0x00,0xff}, //    Up,         //上 　　
                {0x00,0x10,0x00,0xff}, //    Down,       //下 　　
                {0x00,0x04,0xff,0x00}, //    Left,       //左 　　
                {0x00,0x02,0xff,0x00}, //    Right,      //右 
                    
                {0x00,0x0C,0xff,0xff}, //   UpLeft,     //上左
                {0x00,0x0A,0xff,0xff}, //   UpRight,    //上右
                {0x00,0x14,0xff,0xff}, //   DownLeft,   //下左
                {0x00,0x12,0xff,0xff}, //   DownRight,  //下右
  
                {0x00,0x40,0x00,0x00}, //   ZoomAdd,        //变倍+ 
                {0x00,0x20,0x00,0x00}, //   ZoomSub,        //变倍- 　
            　
                {0x00,0x80,0x00,0x00}, //   FocusNear,      //聚焦近 　　
                {0x01,0x00,0x00,0x00}, //   FocusFar,       //聚焦远　
                {0x04,0x00,0x00,0x05}, //   ApertureBig,    //光圈+
                {0x02,0x00,0x00,0x00}, //   ApertureSmall,  //光圈- 　        
　           
                {0x00,0x0b,0x00,0x01}, //   AuxOff,     //辅助开关关 　
                {0x00,0x09,0x00,0x01}, //   AuxOn,      //辅助开关开
                                            
                {0x00,0x07,0x00,0x01}, //   CallPos,    //调用预置点
                {0x01,0x00,0x03,0x00}, //   SetPos,     //设置预置点
                {0x01,0x00,0x05,0x00}, //    DellPos,    //删除预置点
                {0x01,0x00,0x00,0x00}  //    CallStop,   //停止
            };
        }
    }
    /*
    public class PelcoD
    { 
        static byte Header = 0xff;
        static byte[,] CmdData = new byte[20,4]
        {
            {0x00,0x08,0x00,0xff}, //    Up,         //上 　　
            {0x00,0x10,0x00,0xff}, //    Down,       //下 　　
            {0x00,0x04,0xff,0x00}, //    Left,       //左 　　
            {0x00,0x02,0xff,0x00}, //    Right,      //右 
                    
            {0x00,0x0C,0xff,0xff}, //   UpLeft,     //上左
            {0x00,0x0A,0xff,0xff}, //   UpRight,    //上右
            {0x00,0x14,0xff,0xff}, //   DownLeft,   //下左
            {0x00,0x12,0xff,0xff}, //   DownRight,  //下右
  
            {0x00,0x40,0x00,0x00}, //   ZoomAdd,        //变倍+ 
            {0x00,0x20,0x00,0x00}, //   ZoomSub,        //变倍- 　
            　
            {0x00,0x80,0x00,0x00}, //   FocusNear,      //聚焦近 　　
            {0x01,0x00,0x00,0x00}, //   FocusFar,       //聚焦远　
            {0x04,0x00,0x00,0x005}, //   ApertureBig,    //光圈+
            {0x02,0x00,0x00,0x00}, //   ApertureSmall,  //光圈- 　        
　           
            {0x00,0x0b,0x00,0x01}, //   AuxOff,     //辅助开关关 　
            {0x00,0x09,0x00,0x01}, //   AuxOn,      //辅助开关开
                                            
            {0x00,0x07,0x00,0x01}, //   CallPos,    //调用预置点
            {0x01,0x00,0x03,0x00}, //   SetPos,     //设置预置点
            {0x01,0x00,0x05,0x00}, //    DellPos,    //删除预置点
            {0x01,0x00,0x00,0x00} //    CallStop,   //停止
        };

        /// <summary>
        /// 根据命令获取命令数据
        /// </summary>
        /// <param name="data"></param>
        /// <param name="num"></param>
        /// <param name="controlcmd"></param>
        public void GetCmdData(byte[] data, int num, ControlCmd controlcmd)
        {
            int index = (int)controlcmd;
            if (index > 20)
                return;
            for (int i = 0; i < 4; i++)
            { 
                data[i + num] = CmdData[index,i];
            }        
        }
    }
     * */

    class DoControlCmd
    {
        //public ControlProtocal _ControlProtocal;

        public ControlCmd _ControlCmd;
        PelcoD pelcod = new PelcoD();
        PelcoP pelcop = new PelcoP();


        public void GetControlCmd(byte[] data, int num, ControlProtocal ControlProtocal, ControlCmd controlcmd)
        {
            switch(ControlProtocal)
            {
                case ControlProtocal.PelcoD:                    
                    pelcod.GetCmdData(data, num, controlcmd);
                    break;
                case ControlProtocal.PelcoP:
                    pelcop.GetCmdData(data, num, controlcmd);
                    break;
                case ControlProtocal.Hikvision:
                    break;
                case ControlProtocal.Mytool:
                    break;
                default:
                    break;
            }
        }

    }
}
