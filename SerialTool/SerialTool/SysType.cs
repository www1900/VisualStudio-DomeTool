﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialTool
{  
    /// <summary>
    /// 控制协议
    /// </summary>
    public enum ControlProtocal
    {
        PelcoD,
        PelcoP,
        Hikvision,
        Mytool,
    }

    /// <summary>
    /// 文件传输协议
    /// </summary>
    public enum FileProtocal
    {
        Xmodem,
        Ymodem,
        Zmodem,
        Kermit,
    }

    /// <summary>
    /// 控制命令
    /// </summary>
    public enum ControlCmd
    { 
        Up,         //上
        Down,       //下
        Left,       //左
        Right,      //右
        UpLeft,     //上左
        UpRight,    //上右
        DownLeft,   //下左
        DownRight,  //下右
        
        ZoomAdd,        //变倍+
        ZoomSub,        //变倍-
        FocusNear,      //聚焦近
        FocusFar,       //聚焦远
        ApertureBig,    //光圈+
        ApertureSmall,  //光圈-

        AuxOn,      //辅助开关开
        AuxOff,     //辅助开关关
        CallPos,    //调用预置点
        SetPos,     //设置预置点
        DellPos,    //删除预置点
        CallStop,   //停止
    }


    public enum XmodemCmd
    { 
        SOH = 0x01,
        EOT = 0x04,
        ACK = 0x06,
        NAK = 0x15,
        CAN = 0x18,
    }

    public enum YmodemCmd
    { 
        PACKET_SEQNO_INDEX      = (1),
        PACKET_SEQNO_COMP_INDEX = (2),

        PACKET_HEADER           = (3),
        PACKET_TRAILER          = (2),
        PACKET_OVERHEAD         = (PACKET_HEADER + PACKET_TRAILER),
        PACKET_SIZE             = (128),
        PACKET_1K_SIZE          = (1024),

        FILE_NAME_LENGTH        = (256),
        FILE_SIZE_LENGTH        = (16),

        SOH                     = (0x01),  /* start of 128-byte data packet */
        STX                     = (0x02),  /* start of 1024-byte data packet */
        EOT                     = (0x04),  /* end of transmission */
        ACK                     = (0x06),  /* acknowledge */
        NAK                     = (0x15),  /* negative acknowledge */
        CA                      = (0x18),  /* two of these in succession aborts transfer */
        CRC16                   = (0x43),  /* 'C' == 0x43, request 16-bit CRC */

        ABORT1                  = (0x41),  /* 'A' == 0x41, abort by user */
        ABORT2                  = (0x61),  /* 'a' == 0x61, abort by user */

        NAK_TIMEOUT             = (0x100000),
        MAX_ERRORS              = (5),
    }
   
}
